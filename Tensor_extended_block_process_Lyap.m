function [V,H,R0,residu,iter]=Tensor_extended_block_process_Lyap(A,L,U,B,Bmat,m,eps)
a=size(B);
% nrmB=norm(B);
J1=a(1); J2=a(2); K1=a(3);K2=a(4);
residu=[];
% [J1,J2,K1,K2]=size(B);
% I4=Identity4(K1,K2,K1,K2);
matTj=sparse(2*(m+1)*K1*K2,2*m*K1*K2);
K22=2*K2;
V=tensor(zeros(J1,J2,K1,(m+1)*K22));
H=tensor(zeros(K1,(m+1)*K22,K1,m*K22));
%%%%%%%%%%%%%%
% VV=Amt\Bmt;
VV=U\(L\Bmat);
[VV,R0]=qr([Bmat,VV],0);
K1K2=K1*K2;
o11=R0(1:K1K2,1:K1K2);o12=R0(1:K1K2,K1K2+1:2*K1K2);
o22=R0(K1K2+1:2*K1K2,K1K2+1:2*K1K2);
VV=reshape(VV,[J1,J2,K1,K22]); R0=reshape(R0,[K1,K22,K1,K22]);
R10=R0(:,1:K2,:,1:K2);
V(:,:,:,1:K22)=VV;
k=1;
for j=1:m
    W=tensor(zeros(J1,J2,K1,K22));
    jj=(j-1)*K22+1:j*K22;
    %W=ttt(tensor(A),tensor(Vold),[3,4],[1,2]);
    %j1=(j-1)*K2+1:j*K2; j2=j*K2+1:(j+1)*K2;
    j1=1:K2; j2=K2+1:K22;
    W(:,:,:,j1)=ttt(A,tensor(VV(:,:,:,j1)),[3,4],[1,2]);
    matVj2=reshape(VV(:,:,:,j2),[J1*J2,K1*K2]);
%     VV=Amt\double(matVj2);
    VV=U\(L\double(matVj2));
    W(:,:,:,j2)=reshape(VV,[J1,J2,K1,K2]);
    %[W(:,:,:,j2),~]=Gl_GMRES_mod(A,Vj(:,:,:,j2),X0,restrt,max_it,K2);
    for i=1:j
        ii=(i-1)*K22+1:i*K22;
        dd=permute(V(:,:,:,ii),[3,4,1,2]);
        H(:,ii,:,jj)=ttt(dd,W,[3,4],[1,2]);
        W=W-ttt(V(:,:,:,ii),H(:,ii,:,jj),[3,4],[1,2]);
    end
    W=reshape(W,[J1*J2,K1*K22]);
    [VV,R]=qr(double(W),0);
    VV=reshape(VV,[J1,J2,K1,K22]);
    R=reshape(R,[K1,K22,K1,K22]);
    jnew=j*K22+1:(j+1)*K22;
    V(:,:,:,jnew)=VV;
    H(:,jnew,:,jj)=R;

%%Computing Tm=Vm^T*(A*Vm)
Ik2=speye(2*(j+1)*K1K2); 
H1=H(:,1:(j+1)*K22,:,1:j*K22);
matH=reshape(double(H1),[(j+1)*K1*K22,j*K1*K22]);
if j==1
    matTj(1:4*K1K2,1:K1K2)=matH(1:4*K1K2,1:K1K2);

    matTj(1:4*K1K2,K1K2+1:2*K1K2)=(Ik2(:,1:K1K2)*o11-matTj(1:4*K1K2,1:K1K2)*o12)/o22;
else
   H22=matH(2*k*K1K2+1:2*(k+1)*K1K2,2*(k-1)*K1K2+1:2*k*K1K2);
   H22=H22(K1K2+1:2*K1K2,K1K2+1:2*K1K2);
   Ijjk=speye(2*(j+1)*K1K2); 
   matTj(1:2*(j+1)*K1K2,2*(j-1)*K1K2+1:(2*j-1)*K1K2)=matH(1:2*(j+1)*K1K2,2*(j-1)*K1K2+1:(2*j-1)*K1K2);
  
   matTj(1:2*(j+1)*K1K2,(2*k+1)*K1K2+1:(2*k+2)*K1K2)=(Ik2(:,(2*k-1)*K1K2+1:2*k*K1K2)-matTj(1:2*(j+1)*K1K2,1:(2*k+1)*K1K2)*matH(1:(2*k+1)*K1K2,(2*k-1)*K1K2+1:2*k*K1K2))/H22;
   k=k+1;
end

%%Computing the small equation via reshape
    I4j=Identity4(K1,j*K22,K1,j*K22); I1=I4j(:,:,:,1:K2);
    I1R10=mytensorprod(I1,R10,[3,4],[1,2]);
    rhs=mytensorprod(I1R10,permute(I1R10,[3,4,1,2]),[3,4],[1,2]);
%     I1R10=ttt(I1,R10,[3,4],[1,2]);
%     rhs=ttt(I1R10,permute(I1R10,[3,4,1,2]),[3,4],[1,2]);
%     pd=ttt(A,V(:,:,:,1:j*K22),[3,4],[1,2]);
%     Vt=permute(V,[3,4,1,2]);
%     TJ=ttt(Vt,pd,[3,4],[1,2]);
%     Tjj=TJ(:,j*K22+1:(j+1)*K22,:,(j-1)*K22+1:j*K22);
    T=reshape(full(matTj(1:(j+1)*K1*K22,1:j*K1*K22)),[K1,2*(j+1)*K2,K1,2*j*K2]);
    Tjj=T(:,j*K22+1:(j+1)*K22,:,(j-1)*K22+1:j*K22);
    rhs=reshape(rhs,K1*j*K22,K1*j*K22);
    matTjj=reshape(T(:,1:j*K22,:,1:j*K22),[K1*j*K22,K1*j*K22]);
    Yj=dlyap(double(matTjj),rhs);
    Yj=reshape(Yj,[K1,j*K22,K1,j*K22]);

    %%%%%%%% Calcul of the residu %%%%%%%%%%%%%%%%%%%%
    betaj=ttt(tensor(Tjj),tensor(permute(I4j(:,:,:,(j-1)*K22+1:j*K22),[3,4,1,2])),[3,4],[1,2]);
%     betaj=ttt(R,permute(I4j(:,:,:,(j-1)*K2+1:j*K2),[3,4,1,2]),[3,4],[1,2]);
    Yjbetaj=ttt(tensor(Yj),permute(betaj,[3,4,1,2]),[3,4],[1,2]);
%     Yjbetaj=ttt(Yj,permute(betaj,[3,4,1,2]),[3,4],[1,2]);
%     residu11=mytensorprod(H(:,1:j*K2,:,1:j*K2),Yjbetaj,[3,4],[1,2]);
    residu11=ttt(tensor(T(:,1:j*K22,:,1:j*K22)),tensor(Yjbetaj),[3,4],[1,2]);
    residu12=ttt(betaj,Yjbetaj,[3,4],[1,2]);
%     residu12=ttt(betaj,Yjbetaj,[3,4],[1,2]);
    residu=[residu,sqrt(2*norm(tensor(residu11))^2+norm(tensor(residu12))^2)];
    if (residu(end)<eps)
        break,
    end
%     res=sqrt(2*norm(tensor(residu11))^2+norm(tensor(residu12))^2);
end
%T=reshape(full(matTj),[K1,2*(m+1)*K2,K1,2*m*K2]);
iter=j;
end

