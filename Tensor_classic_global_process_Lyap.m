function [V,H,residu,iter]=Tensor_classic_global_process_Lyap(A,B,mmax,eps)
% We solve X-A*X*A'=B*B' with einstein product where 
% A \in \R^{J1 J2 J1 J2} and B \in \R^{J1 J2 K1 K2}
residu=[];
a=size(B);
 J1=a(1); J2=a(2); K1=a(3);K2=a(4);
 %V=sptensor([J1,J2,K1,(mmax+1)*K2]);
 V=tensor(zeros(J1,J2,K1,(mmax+1)*K2));
%  H=sparse(mmax+1,mmax);
% V=zeros(J1,J2,K1,(mmax+1)*K2);
%I4=Identity4(K1,K2,K1,K2);
nrmB=norm(B);nrmB2=nrmB^2;
%H=zeros(m+1,m);
% V=zeros(J1,J2,K1,(m+1)*K2);
Vold=B/nrmB;
V(:,:,:,1:K2)=Vold;
% tic
% Voldt=permute(Vold,[3,4,1,2]);
% nrmBBT=ttt(Vold,Voldt,[3,4],[1,2]);
% nrmBBT=nrmB2*norm(nrmBBT);
% toc
for j=1:mmax
%     W=ttt(tensor(A),tensor(Vold),[3,4],[1,2]);
%    W=mytensorprod(A,Vold,[3,4],[1,2]);
    W=ttt(A,Vold,[3,4],[1,2]);
    for i=1:j
        i1=(i-1)*K2+1:i*K2;
        d=V(:,:,:,i1);
        H(i,j)=d(:)'*W(:);
        W=W-H(i,j)*V(:,:,:,i1);
    end
    Hjj=norm(tensor(W));
    H(j+1,j)=Hjj;
    Vold=W/Hjj;
    j1=j*K2+1:(j+1)*K2;
    V(:,:,:,j1)=Vold;
    %%% Solving the small size Sylv. equation %%%
    Ij=eye(j); e1j=Ij(:,1); 
    rhs= nrmB2*(e1j*e1j');
    HH=H(1:j,1:j);
    Y=dlyap(HH,rhs);
    %%% Computing the residu %%%
    ejj=Ij(:,j); Yejj=Y*ejj;
    res1=HH*Yejj; 
    res2=ejj'*Yejj;
    res=sqrt(2*((Hjj^2)*norm(res1)^2)+(Hjj^4)*res2^2);%/nrmBBT;
    residu=[residu,res];
    if (res<eps)
        break,
    end
end 
iter=j;
end

