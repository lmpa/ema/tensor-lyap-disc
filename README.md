This MATLAB program is designed to solve large-scale discrete Lyapunov tensor equations. We employ projection methods based on tensor Krylov subspaces, specifically the tensor classic and extended Krylov subspaces through the block and global processes.
  
  
THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED WARRANTY.                                                           ANY USE OF THE SOFTWARE CONSTITUTES  ACCEPTANCE OF THE TERMS OF THE ABOVE STATEMENT.

# Authors 
                                           
     M. A. HAMIDI                         
     Université du Littoral, Calais, France
     E-mail: mohamed-amine.hamadi@centralesupelec.fr
     
     K. JBILOU                                                            
     Université du Littoral, Calais, France                               
     E-mail: khalide.jbilou@univ-littoral.fr                                

     A. RATNANI                                                            
     Université polytechnique Mohammed VI, Maroc                             
     