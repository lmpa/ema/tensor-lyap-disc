function result = mytensorprod(A,B,varargin)
% It is similar to the built-in function tensorprod in R2022a
% Differences : mytensorprod does not support singleton
%               mytensorprod supports data type gpuArray
% Advantage :   mytensorprod is much faster than tensorprod
%               by using the GPU computing if A and B are single precision
%               arrays
% The syntax is the same as tensorprod, except no 'NumDimensionsA' for
% singletons
% C = mytensorprod(A,B,dimA,dimB)
% C = mytensorprod(A,B,dim)
% C = mytensorprod(A,B)
% C = mytensorprod(A,B,"all")
% See also : tensorprod
% Copyright : yiy042@ucsd.edu, 2022

narginchk(2,4);

% check whether it is GPU computing and whether there is GPU
% if isgpuarray(A) || isgpuarray(B)
%     if ~parallel.gpu.GPUDevice.isAvailable()
%         error('The GPU could not be used.');
%     end
% end

% check whether A or B is a scalar
if isscalar(A) || isscalar(B)
    result = A.*B;
    return
end

sizeA = size(A); sizeB = size(B);

% check whether A or B have singletons
if ~isequal(sizeA,size(squeeze(A))) || ~isequal(sizeB,size(squeeze(B)))
    error('There exist(s) singleton(s) in A or B');
end

% delete the singleton if A or B is a vector
if isvector(A)
    sizeA = length(A);
end
if isvector(B)
    sizeB=length(B);
end

if nargin == 2
    dimA = [];
    dimB = [];
elseif nargin == 3
    if isnumeric(varargin{1})
        dimA = varargin{1};
        dimB = dimA;
    elseif strcmpi(varargin{1},'all')
        dimA = 1:length(sizeA);
        dimB = 1:length(sizeB);
    else
        error('The 3rd argument is wrong!');
    end
else
    dimA = varargin{1};
    dimB = varargin{2};
end

% outer product
if isempty(dimA)
    if isempty(dimB)
        result = A(:)*B(:)';
        result = reshape(result,[sizeA,sizeB]);
        return
    else
        error('Specified dimensions of the input arrays must have the same sizes.');
    end
end

% check the specified dimensions
if ~isequal(sizeA(dimA),sizeB(dimB))
    error('Specified dimensions of the input arrays must have the same sizes.');
end

dimAouter = 1:length(sizeA); dimAouter(dimA)=[];
dimBouter = 1:length(sizeB); dimBouter(dimB)=[];

% inner product
if isempty(dimAouter)
    Ap = permute(A,dimA);
    if isempty(dimBouter)
        Bp = permute(B,dimB);
        result = dot(Ap(:),Bp(:));
        return
    else
        Bp = permute(B,[dimB,dimBouter]);
        Bpr = reshape(Bp,[prod(sizeB(dimB)),prod(sizeB(dimBouter))]);
        result = Ap(:)'*Bpr;
        if isscalar(dimBouter)
            result = reshape(result,sizeB(dimBouter),1);
        else
            result = reshape(result,sizeB(dimBouter));
        end
        return
    end
end

if isempty(dimBouter)
    Bp = permute(B,dimB);
    Ap = permute(A,[dimAouter,dimA]);
    Apr = reshape(Ap,[prod(sizeA(dimAouter)),prod(sizeA(dimA))]);
    result = Apr*Bp(:);
    if isscalar(dimAouter)
        result = reshape(result,sizeA(dimAouter),1);
    else
        result = reshape(result,sizeA(dimAouter));
    end
    return
end

Ap = permute(A,[dimAouter,dimA]);
Apr = reshape(Ap,[prod(sizeA(dimAouter)),prod(sizeA(dimA))]);
Bp = permute(B,[dimB,dimBouter]);
Bpr = reshape(Bp,[prod(sizeB(dimB)),prod(sizeB(dimBouter))]);

result = Apr*Bpr;
result = reshape(result,[sizeA(dimAouter),sizeB(dimBouter)]);
return
