function [V,R]=global_ortho(A,K2)
%GLOBAL_ORTHO Summary of this function goes here
%   Detailed explanation goes here
V=tensor(zeros(size(A)));
A1=A(:,:,:,1:K2);
A2=A(:,:,:,K2+1:2*K2);
R(1,1)=norm(tensor(A1));
Q1=A1/R(1,1); 
V(:,:,:,1:K2)=Q1;

Qtilde=A2;
R(1,2)=Q1(:)'*Qtilde(:);

Qtilde=Qtilde-R(1,2)*Q1;
R(2,2)=norm(tensor(Qtilde));

V(:,:,:,K2+1:2*K2)=Qtilde/R(2,2);
end

