function resu=KronTens(ymm,I4)
% ymm is a m*m matrix
% I4 is K1*K2*K1*K2 identity tensor
% resu is a K1*mK2*K1*mK2 tensor
[K1,~,~,~,]=size(I4);
for i=1:K1
    resu(i,:,i,:)=kron(ymm,squeeze(I4(i,:,i,:)));
end
end

