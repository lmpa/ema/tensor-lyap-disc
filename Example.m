%This script initiates four methods (Tensor extended block and global)
%(Tensor classic block and global)
%and concludes with plots depicting the residual error and time consumption
% Our goal is to solve X-A*X*A^T=B*B^T a tensorial disc. Lyap. equa. via
% the Einstein product

% Authors : Mohamed Amine HAMADI & Khalide Jbilou & Ahmed Ratnani
% contact : mohamed-amine.hamadi@centralesupelec.fr

%more details could be found in :
%A model reduction method for large-scale linear multidimensional dynamical systems
%https://arxiv.org/abs/2305.09361

clear all,
clc,
%
K1=3;K2=5;

NN=[128,256,512];
%Sparsity
sp=[1e-1,1e-2,1e-3];
for i=1:length(NN)
N=NN(i); 

%Generating the tensors
%%Example__1
%In this example, we have notice that no convergence has been achieved
%when using the tensor classic block and global and more iterations 
%were needed.
%a=0.5/sqrt(N^2);
% AA=speye(N^2)+a*sprandn(N^2,N^2,sp(i));
% N.B.: Consider modifying "NN", the sparsity "sp" and m0, m1 the
% dimensions associated to the tensor Krylov subspaces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Example__2
a=-1-0.5; b=-7; c=-1+0.5;
AA=spdiags([a*ones(N^2,1), b*ones(N^2,1), c*ones(N^2,1)],-1:1,N^2,N^2);


A=spreshape(tensor(AA),[N,N,N,N]);
B=sptenrand([N,N,K1,K2],N*N);
B=tensor(B);

% A=tensor(sptenrand([N,N,N,N],N*N));
% B=tensor(rand(N,N,K1,K2));

% B=tenones([N,N,K1,K2]);
% B=tenrand([N,N,K1,K2]);

%LU Decomposition
tic
[L,U]=lu(double(AA));
tlu=toc;

eps=1e-6;
Bmt=double(reshape(B,[N*N,K1*K2]));

m0=25; % 2*m0 is the dimension of the Ext. Kry. sub.
m1=2*m0; % m1 is the dimension of Classic Kry. sub.

fprintf('Initiating the four methods\n,concluding with plots illustrating \n residual error and execution time');


tic
[~,~,~,residu3,iter3]=Tensor_extended_block_process_Lyap(A,L,U,B,Bmt,m0,eps);
tf_ebTen=toc;
tic
[~,residu4,iter4]=Tensor_extended_global_process_Lyap(A,L,U,B,Bmt,m0,eps);
tf_egTen=toc;
tic
[~,~,residu5,iter5]=Tensor_classic_global_process_Lyap(A,B,m1,eps);
tf_cTen=toc;
tic
 [~,~,residu6,iter6]=Tensor_classic_block_process_Lyap(A,B,m1,eps);
tf_cBTen=toc;ITER6(i)=iter6;

tfebt(i)=tf_ebTen; tfegt(i)=tf_egTen;
tfcten(i)=tf_cTen;tfcBTen(i)=tf_cBTen;


error_EBT(i)=residu3(end);
error_EGT(i)=residu4(end);
error_Ten_Cla(i)=residu5(end);
error_Ten_Cla_block(i)=residu6(end);
end
%%Plotting the obtained results
 semilogx(NN.^2,tfebt,'-*')
 hold on 
 semilogx(NN.^2,tfegt,'-.')
 hold on 
 semilogx(NN.^2,tfcten,'--')
 hold on
 semilogx(NN.^2,tfcBTen)
 legend('Ext.Ten. Bloc','Ext.Ten. Glo','Classic Ten. Glo','Classic Ten. block')
 xlabel('Problem size')
 ylabel('Computational time in sec.')
 %%%%%%%%%%%%%%%
 figure()

 loglog(NN.^2,error_EBT,'-*')
 hold on 
 loglog(NN.^2,error_EGT,'-.')
 hold on 
 loglog(NN.^2,error_Ten_Cla,'--')
 hold on
 loglog(NN.^2,error_Ten_Cla_block)
 legend('Ext.Ten.Bloc','Ext.Ten.Glo','Classic Ten. Glo','Classic Ten. block')
 xlabel('Problem size')
 ylabel('Residual error')
 