function [V,H,residu,iter] =Tensor_classic_block_process_Lyap(A,B,m,eps)
% A is a J1 J2 J1 J2 tensor
% B is a J1 J2 K1 K2 tensor 
% Vm from V is a J1 J2 K1 mK2 tensor
a=size(B);
J1=a(1); J2=a(2); K1=a(3); K2=a(4);
% nrm_B=mytensorprod(B,permute(B,[3,4,1,2]),[3,4],[1,2]);
% nrm_B=norm(tensor(nrm_B));
V=tensor(zeros(J1,J2,K1,(m+1)*K2));
%V=sptensor([J1,J2,K1,(m+1)*K2]);
% H=zeros(K1,(m+1)*K2,K1,m*K2);
H=tensor(zeros(K1,(m+1)*K2,K1,m*K2));
BB=reshape(B,[J1*J2,K1*K2]);
[Q,R10]=qr(double(BB),0); 
Q=reshape(Q,[J1,J2,K1,K2]); 
R10=reshape(R10,[K1,K2,K1,K2]);

V(:,:,:,1:K2)=Q;
for j=1:m
    %j1=(j-1)*K1+1:j*K1; 
    j2=(j-1)*K2+1:j*K2;
%     W=mytensorprod(A,V(:,:,:,j2),[3,4],[1,2]); % J1 J2 K1 K2
    W=ttt(A,V(:,:,:,j2),[3,4],[1,2]);
    for i=1:j
        %i1=(i-1)*K1+1:i*K1;
        i2=(i-1)*K2+1:i*K2;
        dd=permute(V(:,:,:,i2),[3,4,1,2]); % K1 K2 J1 J2
        %HH=mytensorprod(dd,W,[3,4],[1,2]); % K1 K2 K1 K2
%         H(:,i2,:,j2)=mytensorprod(dd,W,[3,4],[1,2]);
        H(:,i2,:,j2)=ttt(dd,W,[3,4],[1,2]);
        %H(i,j)=d(:)'*W(:);
%         W=W-mytensorprod(V(:,:,:,i2),H(:,i2,:,j2),[3,4],[1,2]);
        W=W-ttt(V(:,:,:,i2),H(:,i2,:,j2),[3,4],[1,2]);
    end
    W=reshape(W,[J1*J2,K1*K2]);
    [Q,R]=qr(double(W),0); Q=reshape(Q,[J1,J2,K1,K2]);
    R=reshape(R,[K1,K2,K1,K2]);
    V(:,:,:,j*K2+1:(j+1)*K2)=Q;
    H(:,j*K2+1:(j+1)*K2,:,j2)=R;
    %%%%%%%%%%% Solving the small scale stein equation %%%%%%%%%%%%%%
    I4j=Identity4(K1,j*K2,K1,j*K2); I1=I4j(:,:,:,1:K2);
    I1R10=mytensorprod(I1,R10,[3,4],[1,2]);
    rhs=mytensorprod(I1R10,permute(I1R10,[3,4,1,2]),[3,4],[1,2]);
%     I1R10=ttt(I1,R10,[3,4],[1,2]);
%     rhs=ttt(I1R10,permute(I1R10,[3,4,1,2]),[3,4],[1,2]);
    rhs=reshape(rhs,K1*j*K2,K1*j*K2);
    Hj=reshape(H(:,1:j*K2,:,1:j*K2),[K1*j*K2,K1*j*K2]);
    Yj=dlyap(double(Hj),rhs);
    Yj=reshape(Yj,[K1,j*K2,K1,j*K2]);
    %%%%%%%% Calcul of the residu %%%%%%%%%%%%%%%%%%%%
    betaj=mytensorprod(R,permute(I4j(:,:,:,(j-1)*K2+1:j*K2),[3,4,1,2]),[3,4],[1,2]);
%     betaj=ttt(R,permute(I4j(:,:,:,(j-1)*K2+1:j*K2),[3,4,1,2]),[3,4],[1,2]);
    Yjbetaj=mytensorprod(Yj,permute(betaj,[3,4,1,2]),[3,4],[1,2]);
%     Yjbetaj=ttt(Yj,permute(betaj,[3,4,1,2]),[3,4],[1,2]);
%     residu11=mytensorprod(H(:,1:j*K2,:,1:j*K2),Yjbetaj,[3,4],[1,2]);
    residu11=ttt(H(:,1:j*K2,:,1:j*K2),tensor(Yjbetaj),[3,4],[1,2]);
    residu12=mytensorprod(betaj,Yjbetaj,[3,4],[1,2]);
%     residu12=ttt(betaj,Yjbetaj,[3,4],[1,2]);
    residu(j)=sqrt(2*norm(tensor(residu11))^2+norm(tensor(residu12))^2);%/nrm_B;
    if residu(end)<eps
        break,
    end
end
residu=residu(end); iter=j;
end

