function [A,B,C]=getting_mat_heat_equa(h)
%GETTING_MAT_HEAT_EQUA Summary of this function goes here
%   Detailed explanation goes here
%% d(Phi(t,x)/dt=c^2 delta(Phi(t,x)-c2Phi(t,x)+Dirac u(t)
%%% c2=0.1
    c = 1;
    N = 1/h;
    t = (h^2/(4*c))/2;
    lambda = c*t/h^2;
    
    temp = 2*speye(N);
    for i = 1:N-1
        temp(i, i+1) = -1;
    end
    
    for i = 2:N
        temp(i, i-1) = -1;
    end
    
    A = -0.1*speye(N^2) +speye(N^2) - (kron(temp, speye(N)) + kron(speye(N), temp))*lambda;
    %B = zeros(1, 1, N, N);
    B=zeros(N,N,1,1);
    B(1, 1, 1, 1) = 1;
    %C = zeros(1, 1, N, N);
    C=zeros(N,N,1,1);
    C(1, 1, 1, 1) = 1;
end

